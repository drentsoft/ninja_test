package controllers;

import com.google.inject.Inject;
import dao.ComplexTestDao;
import java.util.List;
import models.ComplexTest;
import models.ComplexTestDto;
import models.ViewingPermission;
import ninja.Context;
import ninja.Result;
import ninja.Results;
import ninja.validation.JSR303Validation;
import ninja.validation.Validation;

/**
 *
 * @author Derwent Ready @ Drentsoft
 */
public class ComplexController {

    @Inject
    ComplexTestDao testDao;

    public Result home() {
        List<ComplexTest> tests = testDao.getTests( 0, 10);
        return Results.html().render("tests", tests);
    }

    public Result newTest() {
        return Results.html().render("viewing_permissions", ViewingPermission.values() );
    }

    public Result postNewTest(Context context,
                                 @JSR303Validation ComplexTestDto testDto,
                                 Validation validation) {
        if (validation.hasViolations()) {
            context.getFlashScope().error("Please correct field." + testDto.title + " : " + testDto.content + " : " + testDto.viewingPermission );
            context.getFlashScope().put("title", testDto.title);
            context.getFlashScope().put("content", testDto.content);
            return Results.redirect("/ctest/new");
        } else {            
            testDao.postTest(context, testDto);
            context.getFlashScope().success("New article created.");            
            //context.getFlashScope().success( test.getTitle() );
            return Results.redirect("/ctest");
        }
    }
    
}
