package controllers;

import com.google.inject.Inject;
import dao.TestDao;
import java.util.List;
import models.Test;
import models.TestDto;
import ninja.Context;
import ninja.Result;
import ninja.Results;
import ninja.validation.JSR303Validation;
import ninja.validation.Validation;

/**
 *
 * @author Derwent Ready @ Drentsoft
 */
public class TestController {
    
    @Inject
    TestDao testDao;

    public Result home() {
        List<Test> tests = testDao.getTests( 0, 10);
        return Results.html().render("tests", tests);
    }

    public Result newTest() {
        return Results.html();
    }

    public Result postNewTest(Context context, //Test test,
                                 @JSR303Validation TestDto testDto,
                                 Validation validation) {
        if (validation.hasViolations()) {
            context.getFlashScope().error("Please correct field.");
            //context.getFlashScope().put("title", testDto.title);
            //context.getFlashScope().put("content", testDto.content);
            return Results.redirect("/test/new");
        } else {            
            testDao.postTest(context, testDto);
            context.getFlashScope().success("New article created.");            
            //context.getFlashScope().success( test.getTitle() );
            return Results.redirect("/test");
        }
    }
    
}
