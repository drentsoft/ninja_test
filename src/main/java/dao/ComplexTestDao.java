package dao;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import models.ComplexTest;
import models.ComplexTestDto;
import ninja.Context;
import ninja.jpa.UnitOfWork;

/**
 *
 * @author Derwent Ready @ Drentsoft
 */
public class ComplexTestDao {
    
    @Inject
    Provider<EntityManager> entitiyManagerProvider;
    
    @UnitOfWork
    public List<ComplexTest> getTests() {
        return getTests(0,10);
    }
    
    @UnitOfWork
    public List<ComplexTest> getTests( int page, int size ) {
        EntityManager entityManager = entitiyManagerProvider.get(); 
        Query query = entityManager.createQuery("SELECT x FROM ComplexTest x WHERE viewingPermission>0 ORDER BY publishDate DESC");
        List<ComplexTest> tests = (List<ComplexTest>) query.setFirstResult(page * size).setMaxResults(size).getResultList();
        return tests;
    }
    
    @UnitOfWork
    public List<ComplexTest> getAllTests( int page, int size ) {
        EntityManager entityManager = entitiyManagerProvider.get(); 
        Query query = entityManager.createQuery("SELECT x FROM ComplexTest x ORDER BY publishDate DESC");
        List<ComplexTest> tests = (List<ComplexTest>) query.setFirstResult(page * size).setMaxResults(size).getResultList();
        return tests;
    }
    
    @Transactional
    public boolean postTest(Context context, ComplexTestDto testDto) {
        System.out.println(context.getMethod());
        EntityManager entityManager = entitiyManagerProvider.get();
        ComplexTest test = new ComplexTest(testDto.title, testDto.content, testDto.viewingPermission );
        entityManager.persist(test);        
        return true;
    }

}
