package dao;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.google.inject.persist.Transactional;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import models.Test;
import models.TestDto;
import ninja.Context;
import ninja.jpa.UnitOfWork;

/**
 *
 * @author Derwent Ready @ Drentsoft
 */
public class TestDao {
   
    @Inject
    Provider<EntityManager> entitiyManagerProvider;
    
    @UnitOfWork
    public List<Test> getTests() {
        return getTests(0,10);
    }
    
    @UnitOfWork
    public List<Test> getTests( int page, int size ) {
        EntityManager entityManager = entitiyManagerProvider.get(); 
        Query query = entityManager.createQuery("SELECT x FROM Test x ORDER BY publishDate DESC");
        List<Test> tests = (List<Test>) query.setFirstResult(page * size).setMaxResults(size).getResultList();
        return tests;
    }
    
    @Transactional
    public boolean postTest(Context context, TestDto testDto) {
        System.out.println(context.getPathParameter("title"));
        EntityManager entityManager = entitiyManagerProvider.get();
        Test test = new Test(testDto.title, testDto.content);
        entityManager.persist(test);        
        return true;
    }

}
