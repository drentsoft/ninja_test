package models;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Derwent Ready @ Drentsoft
 */
public class ComplexTestDto {

    @Size(min = 5)
    public String title;
    
    @Size(min = 5)
    public String content;
    
    @NotNull
    public Integer viewingPermission;
    
    public ComplexTestDto() {}

}
