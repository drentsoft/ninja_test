package models;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author Derwent Ready @ Drentsoft
 */
@Entity
@Table( name = "tests" )
public class Test {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name = "test_id")
    private Long testID;
    
    private String title;
    private String content;
    @Column(name = "publish_date")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date publishDate;
    
    public Test() {}
    
    public Test( String title, String content ) {
        this.title = title;
        this.content = content;
        this.publishDate = new Date();
    }
    
    public String getTitle() {
        return title;
    }
    
    public void setTitle( String title ) {
        this.title = title;
    }
    
    
    
    public String getContent() {
        return content;
    }
    
    public void setContent( String title ) {
        this.content = title;
    }
    
    public Date getPublishDate() {
        return publishDate;
    }
    
}
