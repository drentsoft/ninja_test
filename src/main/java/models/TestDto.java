package models;

import javax.validation.constraints.Size;

/**
 *
 * @author Derwent Ready @ Drentsoft
 */
public class TestDto {

    @Size(min = 5)
    public String title;
    
    @Size(min = 5)
    public String content;
    
    public TestDto() {}
    
}
