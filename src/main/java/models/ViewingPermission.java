/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author Derwent Ready @ Drentsoft
 */
public enum ViewingPermission {
    
    HIDDEN(0, "Hidden"),
    USERS_ONLY(1, "Users only"),
    NO_RESTRICTION(2, "No restriction");
    
    private final int ID;
    private final String name;
    
    ViewingPermission( int ID, String name ) {
        this.ID = ID;
        this.name = name;
    }
    
    public int getID() {
        return ID;
    }
    
    public String getName() {
        return name;
    }
    
    public static ViewingPermission getByID( int ID ) {
        for( ViewingPermission perm : values() ) {
            if( perm.ID == ID )
                return perm;
        }
        return null;
    }
    
}
