package models;

import javax.persistence.AttributeConverter;

/**
 *
 * @author Derwent Ready @ Drentsoft
 */
public class ViewingPermissionConverter implements AttributeConverter<ViewingPermission, Integer> {

    @Override
    public Integer convertToDatabaseColumn(ViewingPermission x) {
        return x.getID();
    }

    /**
     * Convert an integer to a ProjectType object
     */
    @Override
    public ViewingPermission convertToEntityAttribute(Integer id) {
        return ViewingPermission.getByID(id);
    }

}
